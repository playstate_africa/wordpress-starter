# Setup Instructions

- add the remote of the project you're working on:

 `git remote set-url origin https://hostname/USERNAME/REPOSITORY.git`
 
 verify with:
 
 `git remote -v`
 
- Optionally create a git hook to save the local db at .git/hooks/pre-commit

```bash
#!/bin/sh

docker-compose run --rm cli wp db export /database/ethnoresearch-local.sql
echo >&2 "DB backed up to /database/ethnoresearch-local.sql"
echo "./database/ethnoresearch-local.sql" | xargs git add
exit 0
```

`sudo chmod x+ .git/hooks/pre-commit`

- Run the project

    `docker-compose up -d --build`

- Access from the browser:

    wordpress : `0.0.0.0:9090`

- Run wp cli

 `docker-compose run --rm cli bash`

- upgrade

`sudo docker-compose down --rmi all`
`sudo docker-compose up -d`

- backup

`docker-compose run --rm cli wp db export /database/local.sql`

- Restore/import

`docker-compose run --rm cli bash`
`wp db import /database/local.sql`
`wp search-replace 'http://localhost:9090' '[SITE URL]'`
`wp search-replace '[SITE URL]' 'http://localhost:9090'`

- An Example of an nginx proxy:

```nginx
server {
        listen 80;
        listen [::]:80;
        server_name newsite.playstateprojects.com;
        client_max_body_size 20M;

        location / {
                proxy_set_header X-Real-IP  $remote_addr;
                proxy_set_header X-Forwarded-For $remote_addr;
                proxy_set_header Host $host;
                proxy_pass http://127.0.0.1:9091;
        }
}
```
- Run wp cli

	`docker-compose run --rm cli bash`

- upgrade

	`sudo docker-compose down --rmi all`
	`sudo docker-compose up -d`

- backup

	`docker-compose run --rm cli wp db export /database/local.sql`

- Restore/import

	`docker-compose run --rm cli bash`
	`wp db import /database/local.sql`
	`wp search-replace 'http://localhost:9092' 'https://staging.abacus-insurance.co.za'`
	`wp search-replace 'https://staging.abacus-insurance.co.za' 'http://localhost:9090'`
	`wp search-replace 'https://some-site' 'http://localhost:9090' `

### Setting up pipelines.

- enable pipelines in the repo's settings.
- generate an SSH key in the repo settings.
- add the public key to ~/.ssh/authorized_keys of the user account bitbucket will use to transfer files.
- Fetch the hosts fingerprint using known hosts.

